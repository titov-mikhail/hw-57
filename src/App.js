import React from "react";
import BillBuilder from "./containers/BillBuilder";

const App = () => (
   <BillBuilder/>
);

export default App;
