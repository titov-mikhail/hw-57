import React from "react";
import BillEntryControl from "../components/BillControls/BillEntryControl";

const BillFooter = props => {
    return(
        <>
            <BillEntryControl
                label =  'Процент чаевых'
                controlId = 'servicePercent'
                measureName = '%'
                defaultValue ={15}
                onChanged = {(x)=>props.onServiceRateChanged(x)}
            />
            <BillEntryControl
                label =  'Доставка'
                controlId =  'deliveryCost'
                measureName = 'сом'
                defaultValue ={0}
                onChanged = {(x)=>props.onDeliveryCostChanged(x)}
            />
        </>
    );
}

export default BillFooter;