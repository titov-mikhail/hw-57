import React from "react";
import BillEntryControl from "../components/BillControls/BillEntryControl";
import './styles.css';
import BillFooter from "./BillFooter";

const CommonBill = props => {
    return (
        <div className="CommonBill">
            <BillEntryControl
                label = 'Человек'
                controlId = 'peopleCount'
                measureName = 'чел.'
                defaultValue ={props.clientsCount}
                onChanged = {(x)=>props.onClientsCountChanged(x)}

            />
            <BillEntryControl
                label = 'Сумма заказа'
                controlId ='orderSum'
                measureName = 'сом'
                defaultValue ={props.orderSum}
                onChanged = {(x)=>props.onOrderSumChanged(x)}
            />
            <BillFooter
                onServiceRateChanged = {props.onServiceRateChanged}
                onDeliveryCostChanged = {props.onDeliveryCostChanged}
            />
        </div>

    );
}

export default CommonBill;