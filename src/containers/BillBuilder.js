import React, {useState} from 'react';
import CommonBill from "./CommonBill";
import IndividualBill from "./IndividualBill";
import BillTotalControl from "../components/BillControls/BillTotalControl";
import BillingModeControl from "../components/BillingModeControl";
import {nanoid} from "nanoid";

const BillBuilder = () => {
    const [individualMode, setIndividualMode] = useState(false);
    const [clientsCount, setClientCount] = useState(1);
    const [orderSum, setOrderSum] = useState(0);
    const [totalSum, setTotalSum] = useState(0);
    const [deliveryCost, setDeliveryCost] = useState(0);
    const [serviceRate, setServiceRate] = useState(15);
    const [clients, setClients] = useState([]);

    const onModeChanged = isIndividual => {
        setIndividualMode(isIndividual);
    }

    const onClientsCountChanged = count => {
        if(isNaN(count)) {
            setClientCount(0);
        }
        setClientCount(count);
    }

    const calculateCommonBill = () => {
      const newTotalBill = Math.ceil(orderSum + (orderSum * serviceRate/100.0) + deliveryCost);
      setTotalSum(newTotalBill);
    }

    const onServiceRateChanged = rate => {
        if(isNaN(rate)){
            setServiceRate(0);
        }
        setServiceRate(rate);
    }

    const onDeliveryCostChanged = (cost) => {
        if(isNaN(cost)){
            setDeliveryCost(0);
        }
        setDeliveryCost(cost);
    }

    const onOrderSumChanged =  sum => {
        if(isNaN(sum)){
            setOrderSum(0);
        }

        setOrderSum(sum);
    }

    const onClientOrderSumChanged = (clientId, clientOrderSum) => {
        const clientsCopy = [...clients];
        const index = clientsCopy.findIndex(x=>x.clientId === clientId);
        clientsCopy[index].orderSum = clientOrderSum;
        setClients(clientsCopy);
    }

    const addClient = () => {
        setClients([...clients, {clientId: nanoid(), name: '', orderSum: 0, totalSum: 0}])
    }

    const calculateIndividualBills = () => {

        if(clients.length === 0) return;

        const clientDeliveryCost = deliveryCost / clients.length;
        let allClientsTotalSum = 0;

        const newClients = clients.map(x=> {
            const clientTotalSum = Math.ceil(x.orderSum * serviceRate / 100 + x.orderSum +  clientDeliveryCost);

            allClientsTotalSum += clientTotalSum;
            return {
                clientId: x.clientId,
                name: x.name,
                orderSum: x.orderSum,
                totalSum: clientTotalSum
            }
        });

        setClients(newClients);
        setTotalSum(allClientsTotalSum);
    }

    const onClientRemoved = (clientId) => {
        console.log("onClientsRemoved");
        const clientsCopy = [...clients];
        const index = clientsCopy.findIndex(x => x.clientId === clientId);
        console.log(index);
        clientsCopy.splice(index, 1);

        setClients(clientsCopy);
        //calculateIndividualBills(clientsCopy, serviceRate, deliveryCost);
    }

    const  onClientNameChanged = (clientId, newName) => {
        const newClients = clients.map(x=>{
            if(x.clientId === clientId) {
                return {name: newName, orderSum: x.orderSum, totalSum: x.totalSum, clientId: x.clientId}
            }

            return x;
        });

        setClients(newClients);
    }

    const calculateBill = () => {
      individualMode === true ? calculateIndividualBills() : calculateCommonBill();
    }

    return (
        <div className="MainPage">
            <BillingModeControl
                individual = {individualMode}
                onChange = { (x) => onModeChanged(x) }
            />
            {
                individualMode === true
                    ? <IndividualBill
                            clients ={clients}
                            onClientAdded = {()=>addClient()}
                            onClientNameChanged = {(clientId,newName)=>onClientNameChanged(clientId, newName)}
                            onOrderSumChanged = {(clientId, clientOrderSum)=> onClientOrderSumChanged(clientId, clientOrderSum)}
                            onClientRemoved = {(x)=>onClientRemoved(x)}
                            onServiceRateChanged = {(x)=>onServiceRateChanged(x)}
                            onDeliveryCostChanged = {(x)=>onDeliveryCostChanged(x)}
                        />
                    :
                    <CommonBill
                        orderSum ={orderSum}
                        clientsCount ={clientsCount}
                        onOrderSumChanged = {(x)=>onOrderSumChanged(x)}
                        onClientsCountChanged = {(x)=>onClientsCountChanged(x)}
                        onServiceRateChanged = {(x)=>onServiceRateChanged(x)}
                        onDeliveryCostChanged = {(x)=>onDeliveryCostChanged(x)}
                    />
            }
            <button
                className="BtnCalculate"
                onClick={()=>calculateBill()}>
                Рассчитать
            </button>
            <BillTotalControl
                individual = {individualMode}
                totalSum = {totalSum}
                peopleCount = {clientsCount}
                dividedSum = {clientsCount === 0 ? 0: Math.ceil(totalSum / clientsCount)}
                clients = {clients}
            />
       </div>
    );
}

export  default BillBuilder;