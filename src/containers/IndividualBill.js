import React from "react";
import RemovableBillEntryControl from "../components/BillControls/RemovableBillEntryControl";
import BillFooter from "./BillFooter";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPlusSquare} from "@fortawesome/free-regular-svg-icons";

const IndividualBill = props => {
    return (
        <div className="IndividualBill">
            {
                props.clients.map(client => (
                    <RemovableBillEntryControl
                        key = {"Indbill" + client.clientId}
                        client = {client}
                        measureName = 'сом'
                        onRemoved = {()=>props.onClientRemoved(client.clientId)}
                        onOrderSumChanged = {(orderSum)=>props.onOrderSumChanged(client.clientId, orderSum)}
                        onClientNameChanged = {(x)=> props.onClientNameChanged(client.clientId, x)}
                    />
                    ))
            }
            <button
                key={"BtnAddClient"}
                className="BtnAddClient"
                onClick = {()=>props.onClientAdded()}>
                <FontAwesomeIcon icon = {faPlusSquare} />
            </button>
            <BillFooter
                onServiceRateChanged = {(x)=>props.onServiceRateChanged(x)}
                onDeliveryCostChanged = {(x)=>props.onDeliveryCostChanged(x)}
            />
        </div>
    )
}

export default IndividualBill;