import React from "react";

const BillingModeControl = props => {
    return(
        <div className="BillingModeControl">
            <p>Сумма заказа считается:</p>
            <div>
                <input
                    type="radio"
                    name="mode"
                    checked={props.individual === false}
                    id="equalRadio" onChange={()=>props.onChange(false)}
                />
                <label htmlFor="equalRadio">Поровну между участниками</label>

                <input
                    type="radio"
                    name="mode"
                    checked={props.individual === true}
                    id="individualRadio"
                    onChange={()=>props.onChange(true)}
                />
                <label htmlFor="individualRadio">Каждому индивидуально</label>
            </div>
        </div>
    );
}

export default BillingModeControl;