import React from "react";
import './BillEntryControl.css';

const BillEntryControl = props => {
    return (
        <div className="BillEntryControl">
            <div className="Label">
                {props.label}
            </div>
            <div className="BillInput">
                <label>
                    <input
                        type="number"
                        name={props.controlId}
                        key={props.controlId}
                        defaultValue={props.defaultValue}
                        onChange={(e)=>props.onChanged(parseInt(e.target.value))}
                    />
                    {props.measureName}
                </label>
            </div>
        </div>
    );
}

export default BillEntryControl;