import React from "react";

const BillTotalControl = props => {
   if( props.individual === true) {
       return (
           <div className="BillTotalControl">
               <p key="individualBill">Индивидуальный счет</p>
               <p key="totalSum">Общая сумма: <strong>{props.totalSum}</strong> сом</p>
               {props.clients.map(client=>(
                   <p key={"clientTotal"+client.clientId}>{client.name} <strong>{client.totalSum}</strong> сом</p>
               ))}
           </div>
       );
   }
    return (
        <div className="BillTotalControl">
            <p key = "commonBill">Общий счет</p>
            <p key = "commonSum">Общая сумма: <strong>{props.totalSum}</strong> сом</p>
            <p key = "clientsCount">Количество человек: <strong>{props.peopleCount}</strong></p>
            <p key = "dividedSum">Каждый платит по: <strong>{props.dividedSum}</strong> сом</p>
        </div>
    )
}

export default BillTotalControl;