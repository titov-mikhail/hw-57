import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTrashAlt} from "@fortawesome/free-regular-svg-icons";
import './BillEntryControl.css';

const RemovableBillEntryControl = props => {
    return (
        <div className="RemovableBillEntryControl" key={"rmc"+props.client.clientId}>
            <div>
                <label className="Label">
                    Клиент
                    <input
                        className="ClientName"
                        type="text"
                        name={"clientName" + props.client.clientId}
                        key={"clientName" + props.client.clientId}
                        defaultValue={props.client.name}
                        onChange={(e)=>props.onClientNameChanged(e.target.value)}
                    />
                </label>
                <label className="IndivClientLabel">
                    <input type="number"
                           name={"orderSum" + props.client.clientId}
                           key={"orderSum" + props.client.clientId}
                           defaultValue={props.client.orderSum}
                           onChange={(e)=>props.onOrderSumChanged(parseInt(e.target.value))}
                    />
                    {props.measureName}
                </label>
                <button onClick={()=>props.onRemoved(props.client.clientId)}>
                    <FontAwesomeIcon icon={faTrashAlt}/>
                </button>
            </div>

        </div>
    )
}

export default RemovableBillEntryControl;